export const BaseUrl = "http://localhost:8000/api";

export const postRequest = async (url, body) => {
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body,
  });
  const data = await response.json();

  if (!response.ok || data?.code >= 400) {
    let message;
    if (data?.message) {
      message = data?.message;
    } else {
      message = data;
    }
    return { error: true, message };
  }

  return data;
};

export const getRequest = async (url) => {
  const response = await fetch(url);
  const data = await response.json();

  if (!response.ok) {
    let message = "An error occured...";
    if (data?.message) {
      message = data.message;
    }
    return { error: true, message };
  }
  return data;
};

export const postRequestAuth = async (url, body) => {
  const accessToken = JSON.parse(localStorage.getItem("access_token_driver"));
  const refreshToken = JSON.parse(localStorage.getItem("refresh_token_driver"));
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
      Authorization: `${accessToken}`,
    },
    body,
  });
  const data = await response.json();
  if (response.status == 401 && data?.code == 1) {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({ refreshToken }),
    });
    const data = await response.json();
    if (!response.ok || data?.code >= 400) {
      return {
        error: true,
        message: "Thất bại khi sử dụng Refresh Token để lấy token mới",
      };
    }
    localStorage.setItem(
      "access_token_driver",
      JSON.stringify(data.metadata.access_token)
    );

    //Gửi lại request khi có yêu cầu mới
    const newResponse = await fetch(url, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: data.metadata.access_token,
      },
      body,
    });
    const Newdata = await response.json();

    if (!response.ok || Newdata?.code >= 400) {
      let message;
      if (Newdata?.message) {
        message = Newdata?.message;
      } else {
        message = Newdata;
      }
      return { error: true, message };
    }

    return Newdata;
  } else if (response.status == 401 && data?.code == 0) {
    return {
      error: true,
      message: "Access Token không hợp lệ, điều hướng chuyển qua login",
    };
  }

  return data;
};

export const getRequestAuth = async (url) => {
  const accessToken = JSON.parse(localStorage.getItem("access_token_driver"));

  if (!accessToken) {
    return { error: true, message: "Access token không tồn tại." };
  }

  const response = await fetch(url, {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      Authorization: `${accessToken}`,
    },
  });

  const data = await response.json();

  if (response.status === 401 && data?.code === 1) {
    const refreshToken = JSON.parse(
      localStorage.getItem("refresh_token_driver")
    );

    if (!refreshToken) {
      return {
        error: true,
        message: "Refresh token không tồn dại, điều hướng đăng nhập lại",
      };
    }

    const refreshResponse = await fetch(`${BaseUrl}/users/refresh-token`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({ refreshToken }),
    });

    const refreshData = await refreshResponse.json();

    if (!refreshResponse.ok || refreshData?.code >= 400) {
      return {
        error: true,
        message: "Thất bại khi sử dụng Refresh Token để lấy token mới",
      };
    }

    localStorage.setItem(
      "access_token_driver",
      JSON.stringify(refreshData.metadata.access_token)
    );

    // Gửi lại request khi có yêu cầu mới
    const newResponse = await fetch(url, {
      method: "GET",
      headers: {
        "Content-type": "application/json",
        Authorization: refreshData.metadata.access_token,
      },
    });

    const newData = await newResponse.json();

    if (!newResponse.ok || newData?.code >= 400) {
      let message;
      if (newData?.message) {
        message = newData?.message;
      } else {
        message = newData;
      }
      return { error: true, message };
    }

    return newData;
  } else if (response.status === 401 && data?.code === 0) {
    return {
      error: true,
      message: "Access Token không hợp lệ, điều hướng chuyển qua login",
    };
  }

  return data;
};
