import { useContext, useState } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { AuthContext } from "./context/AuthContext";
import Home from "./pages/Home";
import Login from "./pages/Login";
import { DriverContextProvider } from "./context/DriverContext";
import DriverProfile from "./pages/DriverProfile";
import Intrip from "./pages/Intrip";
import Action from "./pages/Action";

function App() {
  const { user } = useContext(AuthContext);
  return (
    <DriverContextProvider user={user}>
      <Routes>
        <Route path="/" element={user ? <Home /> : <Login />} />
        <Route path="/login" element={user ? <Home /> : <Login />} />
        <Route path="/profile" element={user ? <DriverProfile /> : <Login />} />
        <Route path="/intrip" element={user ? <Intrip /> : <Login />} />
        <Route path="/action" element={user ? <Action /> : <Login />} />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </DriverContextProvider>
  );
}

export default App;
