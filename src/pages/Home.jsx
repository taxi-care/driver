import React, { useContext } from "react";
import { DriverContext } from "../context/DriverContext";
import MapboxMap from "../components/Map/MapboxMap";
import Revenue from "../components/Home/Revenue";
import DriverSetting from "../components/Home/DriverSetting";
import NewTrip from "../components/Home/NewTrip";

function Home() {
  const { newTrip, errorAcceptTrip } = useContext(DriverContext);
  return (
    <div className="w-screen h-screen relative">
      <MapboxMap />
      <div className="absolute w-full z-10 flex justify-between ">
        <Revenue />
        <DriverSetting />
      </div>
      {errorAcceptTrip ? (
        <div className="absolute w-full z-10 top-28 flex justify-end">
          <p className="bg-red-500 p-3 rounded-md text-white">
            {errorAcceptTrip.message}
          </p>
        </div>
      ) : null}
      <div className="absolute inset-x-0 bottom-5 flex justify-center z-10">
        <p className="w-[90%] p-3 rounded-md bg-slate-50 shadow-md flex gap-5 items-center">
          <span className="w-2 h-2 bg-green-600 rounded-full inline-block"></span>
          <span> Bạn đang online sẵn sàng nhận cuốc...</span>
        </p>
      </div>
      {newTrip ? <NewTrip /> : null}
    </div>
  );
}

export default Home;
