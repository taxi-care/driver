import React, { useContext } from "react";
import { DriverContext } from "../context/DriverContext";
import TripEnd from "../components/Action/TripEnd";
import { useNavigate } from "react-router-dom";

function Action() {
  const { Revenue, allTripCompleted } = useContext(DriverContext);
  const navigate = useNavigate();
  const handleBackToHome = () => {
    navigate("/");
  };
  return (
    <div>
      <div className="mt-5 ml-3 text-xl font-semibold">
        <h1 className="flex items-center gap-3">
          <img
            src="/back.svg"
            alt="back"
            className="w-5 h-5 cursor-pointer"
            onClick={handleBackToHome}
          />
          <span>Thu nhập</span>
        </h1>
      </div>
      <div className="flex flex-col items-center mt-5">
        <div className="w-[95%] shadow-lg p-3 rounded-md flex flex-col gap-3 bg-slate-200">
          <h1>Tổng thu nhập</h1>
          <p className="font-semibold text-xl">
            {(Revenue * 1000).toLocaleString("vi-VN", {
              style: "currency",
              currency: "VND",
            })}
          </p>
        </div>
      </div>
      <div className="flex flex-col items-center mt-3">
        <div className="w-[95%]">
          <h1 className="text-lg font-semibold">
            Cuốc xe đã hoàn tất: {allTripCompleted?.length}
          </h1>
          <div className="mt-3 flex flex-col items-center gap-5 pb-5">
            {allTripCompleted ? (
              allTripCompleted?.map((item, index) => {
                return <TripEnd key={index} trip={item} />;
              })
            ) : (
              <h1 className="text-xl">Chưa có chuyến đi nào</h1>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Action;
