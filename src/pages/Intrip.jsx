import React, { useContext, useEffect } from "react";
import MapboxMap from "../components/Map/MapboxMap";
import InforTrip from "../components/Intrip/InforTrip";
import { DriverContext } from "../context/DriverContext";
import { useNavigate } from "react-router-dom";

function Intrip() {
  const { currentTrip } = useContext(DriverContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (!currentTrip) {
      navigate("/");
    }
  }, [currentTrip, navigate]);

  return (
    <div className="w-screen h-screen relative">
      <MapboxMap />
      {currentTrip ? <InforTrip /> : null}
    </div>
  );
}

export default Intrip;
