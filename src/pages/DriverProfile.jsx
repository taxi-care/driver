import React, { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { Link } from "react-router-dom";

function DriverProfile() {
  const { user, logoutUser } = useContext(AuthContext);
  return (
    <div className="w-full h-full bg-gray-200">
      <div className="flex flex-col items-center bg-orange-100 relative">
        <Link to="/">
          <div className="absolute top-5 left-5 p-2 border border-gray-600 rounded-md">
            <img src="/back.svg" alt="back to home" className="w-6 h-6" />
          </div>
        </Link>
        <div className="pt-10">
          <img
            src="/DriverProfile.svg"
            alt="avatar user"
            className="w-20 h-20"
          />
        </div>
        <h1 className="font-semibold my-3">{user.name}</h1>
        <h1>{user.phone}</h1>
      </div>
      <div className="">
        <p className="my-2 bg-white p-3">Hỗ trợ</p>
        <p className="my-2 bg-white p-3">Cài đặt</p>
        <p className="my-2 bg-white p-3">Vay vốn</p>
        <Link
          to="/login"
          onClick={() => logoutUser()}
          className="link-light text-decoration-none"
        >
          <p className="my-2 bg-white p-3">Đăng xuất</p>
        </Link>
      </div>
    </div>
  );
}

export default DriverProfile;
