import React, { createContext, useState, useEffect, useRef } from "react";
import { LocationsFake } from "../data/Locations";
import { BaseUrl, postRequest } from "../utils/services";
import { io } from "socket.io-client";
import { useNavigate } from "react-router-dom";
import { useMemo } from "react";

export const DriverContext = createContext();

export const DriverContextProvider = ({ children, user }) => {
  const navigate = useNavigate();
  const [newTrip, setNewTrip] = useState();
  const [currentTrip, setCurrentTrip] = useState();
  const [driverLocation, setDriverLocation] = useState({
    lng: 106.784138,
    lat: 10.844177,
  });
  const [socket, setSocket] = useState(null);
  const [allTripCompleted, setAllTripCompleted] = useState();
  const [errorAcceptTrip, setErrorAcceptTrip] = useState();
  const socketRef = useRef(null);

  //Fake driver move
  useEffect(() => {
    let intervalId;
    if (user) {
      intervalId = setInterval(async () => {
        const randomIndex = Math.floor(Math.random() * LocationsFake.length);
        const [lng, lat] = LocationsFake[randomIndex];
        const newDriverLocation = { lat, lng };
        const res = await postRequest(
          `${BaseUrl}/driver-location/update`,
          JSON.stringify({
            driverId: user._id,
            coordinates: [lng, lat],
            carModelId: user.car_model_id,
            licensePlate: user.license_plate,
          })
        );
        if (res?.error) {
          console.log(res);
        } else {
          console.log("Cập nhật vị trí tài xế thành công");
          setDriverLocation(newDriverLocation);
        }
      }, 50000);
    }

    return () => {
      if (intervalId) {
        clearInterval(intervalId);
      }
    };
  }, [user]);

  useEffect(() => {
    let attempts = 0;

    const connectSocket = () => {
      socketRef.current = io("http://localhost:8000");

      socketRef.current.on("connect", () => {
        console.log("Kết nối socket thành công");
        setSocket(socketRef.current);
        console.log(socketRef.current);
        attempts = 0;
      });

      socketRef.current.on("connect_error", () => {
        console.log("Kết nối socket thất bại, đang thử lại...");
        attempts++;
        if (attempts < 5) {
          setTimeout(connectSocket, 1000); // Thử lại sau 1 giây
        }
      });
    };

    if (user) {
      connectSocket();
    }
    return () => {
      socketRef?.current?.disconnect();
    };
  }, [user]);

  //Add online users
  useEffect(() => {
    if (socket === null) {
      return;
    }
    socket.emit("addNewUser", {
      userId: user?._id,
      role: "driver",
      status: "active",
    });
  }, [socket]);

  //receive notification new trip
  useEffect(() => {
    if (socket === null) {
      return;
    }
    socket.on("newTrip", (res) => {
      setNewTrip(res?.trip);
      console.log("có chuyến đi mới", res.trip);
    });

    return () => {
      socket.off("newTrip");
    };
  }, [socket]);

  //Update status driver socket
  useEffect(() => {
    if (socket === null) {
      return;
    }
    if (currentTrip) {
      socket.emit("updateStatusDriver", {
        userId: user?._id,
        status: "intrip",
      });
    } else {
      socket.emit("updateStatusDriver", {
        userId: user?._id,
        status: "active",
      });
    }
  }, [currentTrip]);

  //Send realtime location for customer on trip
  useEffect(() => {
    if (currentTrip) {
      socket.emit("locationDriverInTrip", {
        userId: currentTrip.userId,
        driverLocation,
      });
    }
  }, [currentTrip, driverLocation]);

  //Driver redirect to page intrip if have a trip
  useEffect(() => {
    if (currentTrip) {
      navigate("/intrip");
    }
  }, [currentTrip]);

  //Driver have trip
  useEffect(() => {
    const fetchData = async () => {
      if (user) {
        const res = await postRequest(
          `${BaseUrl}/trips/user-have-trip`,
          JSON.stringify({ driverId: user?._id })
        );
        if (res.error) {
          return console.log(res);
        }
        if (res.trips.length > 0) {
          setCurrentTrip(res.trips[0]);
        }
      }
    };

    fetchData();
  }, [user]);

  // Get All Trips for Driver
  useEffect(() => {
    const getAllTrips = async () => {
      const res = await postRequest(
        `${BaseUrl}/trips/get-all-trip`,
        JSON.stringify({ driverId: user?._id })
      );
      if (res.error) {
        return console.log(res);
      }
      setAllTripCompleted(res.metadata);
    };
    getAllTrips();
  }, [user, currentTrip]);

  //Calculate driver Revenue
  const Revenue = useMemo(() => {
    if (allTripCompleted) {
      return allTripCompleted.reduce((accumulator, currentItem) => {
        if (currentItem.status === "complete") {
          return accumulator + currentItem.price;
        }
        return accumulator;
      }, 0);
    }
    return 0;
  }, [allTripCompleted]);

  //Handle accept trip
  const handleAcceptTrip = async () => {
    const res = await postRequest(
      `${BaseUrl}/trips/accept`,
      JSON.stringify({ tripId: newTrip._id, driverId: user._id })
    );
    if (res.error) {
      setErrorAcceptTrip(res);
      setNewTrip();
      return;
    }
    console.log("Nhận chuyến đi thành công: Let's go", res.metadata);
    setCurrentTrip(res.metadata);
    setNewTrip();
  };

  //Handle modal error accept code
  useEffect(() => {
    let timer;

    if (errorAcceptTrip) {
      timer = setTimeout(() => {
        setErrorAcceptTrip(null);
      }, 2000);
    }

    return () => {
      if (timer) clearTimeout(timer);
    };
  }, [errorAcceptTrip]);

  //Handle complete trip
  const handleCompleteTrip = async () => {
    const res = await postRequest(
      `${BaseUrl}/trips/update-status`,
      JSON.stringify({
        tripId: currentTrip._id,
        newStatus: "complete",
      })
    );
    if (res.error) {
      return console.log(res);
    }
    setCurrentTrip();
    navigate("/");
    console.log(res);
  };

  //Handle cancel trip
  const handleCancelTrip = async () => {
    const res = await postRequest(
      `${BaseUrl}/trips/driver-cancel`,
      JSON.stringify({
        tripId: currentTrip._id,
        driverId: user._id,
      })
    );
    if (res.error) {
      return console.log(res);
    }
    setCurrentTrip();
    navigate("/");
    console.log("Hủy chuyến đi thành công", res);
  };
  return (
    <DriverContext.Provider
      value={{
        driverLocation,
        newTrip,
        setNewTrip,
        handleAcceptTrip,
        currentTrip,
        handleCompleteTrip,
        Revenue,
        allTripCompleted,
        errorAcceptTrip,
        handleCancelTrip,
      }}
    >
      {children}
    </DriverContext.Provider>
  );
};
