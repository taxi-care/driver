import React from "react";
import moment from "moment";

function TripEnd({ trip }) {
  return (
    <div className="bg-white rounded-md flex flex-col justify-center shadow-md w-[95%]  p-3">
      <div className="flex items-center border-b border-gray-200 gap-3">
        <img src={`/customer.svg`} alt="" className="w-8 h-8" />
        <div className="flex grow justify-between items-center">
          <h1 className="flex flex-col">
            <span>{moment(trip?.createdAt).calendar()}</span>
            <span className="font-semibold">{trip?.price}k</span>
          </h1>
          <span
            className={`p-1 rounded-md ${
              trip.status == "cancel"
                ? "bg-red-200 text-red-500"
                : "bg-green-200 text-green-500"
            } `}
          >
            {trip.status}
          </span>
        </div>
      </div>
      <p className="text-ellipsis whitespace-nowrap overflow-hidden mt-4">
        {trip?.source}
      </p>
      <p className="text-ellipsis whitespace-nowrap overflow-hidden mt-2">
        {trip?.destination}
      </p>
    </div>
  );
}

export default TripEnd;
