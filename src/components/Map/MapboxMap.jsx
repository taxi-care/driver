import { useContext, useRef, useEffect, useState } from "react";
import Map from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { DriverContext } from "../../context/DriverContext";
import Markers from "./Markers";
import MapboxRoute from "./MapboxRoute";

const KEY_MAP = import.meta.env.VITE_KEY_MAP;

function MapboxMap() {
  const mapRef = useRef();
  const [directionData, setDirectionData] = useState();
  const { driverLocation, currentTrip } = useContext(DriverContext);

  //Map fly location driver
  useEffect(() => {
    if (driverLocation) {
      mapRef.current?.flyTo({
        center: [driverLocation.lng, driverLocation.lat],
        duration: 2500,
      });
    }
  }, [driverLocation]);

  useEffect(() => {
    const getDirectionRoute = async () => {
      const res = await fetch(
        "https://api.mapbox.com/directions/v5/mapbox/driving/" +
          currentTrip?.sourceCoordinates[0] +
          "," +
          currentTrip?.sourceCoordinates[1] +
          ";" +
          currentTrip?.destinationCoordinates[0] +
          "," +
          currentTrip?.destinationCoordinates[1] +
          "?overview=full&geometries=geojson" +
          `${KEY_MAP}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      const result = await res.json();
      setDirectionData(result);
    };

    if (currentTrip) {
      getDirectionRoute();
    }
  }, [currentTrip]);

  return (
    <div className="w-full h-full overflow-hidden absolute z-10">
      {driverLocation ? (
        <Map
          ref={mapRef}
          mapboxAccessToken={`${KEY_MAP}`}
          initialViewState={{
            longitude: driverLocation?.lng,
            latitude: driverLocation?.lat,
            zoom: 13,
          }}
          style={{ width: "100%", height: "100%" }}
          mapStyle="mapbox://styles/mapbox/streets-v9"
        >
          <Markers />
          {currentTrip ? (
            <MapboxRoute
              coordinates={directionData?.routes[0]?.geometry?.coordinates}
            />
          ) : null}
        </Map>
      ) : null}
    </div>
  );
}

export default MapboxMap;
