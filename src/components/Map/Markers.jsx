import React, { useContext } from "react";
import { Marker } from "react-map-gl";
import { DriverContext } from "../../context/DriverContext";

function Markers() {
  const { driverLocation, currentTrip } = useContext(DriverContext);
  return (
    <div>
      <Marker
        longitude={driverLocation?.lng}
        latitude={driverLocation?.lat}
        anchor="bottom"
      >
        <div className="pt-10">
          <img src="/locationPinDriver.svg" className="w-14 h-14" />
        </div>
      </Marker>

      {/*Source Makers */}
      {currentTrip ? (
        <Marker
          longitude={currentTrip.sourceCoordinates[0]}
          latitude={currentTrip.sourceCoordinates[1]}
          anchor="bottom"
        >
          <div className="p-16  rounded-full relative top-16">
            <img
              src="/source-location.svg"
              className="w-10 h-10 animate-bounce"
            />
          </div>
        </Marker>
      ) : null}
      {/*Destination Makers */}
      {currentTrip ? (
        <Marker
          longitude={currentTrip.destinationCoordinates[0]}
          latitude={currentTrip.destinationCoordinates[1]}
          anchor="bottom"
        >
          <img src="/book-location.svg" className="w-10 h-10" />
        </Marker>
      ) : null}
    </div>
  );
}

export default Markers;
