import React, { useContext, useState } from "react";
import { DriverContext } from "../../context/DriverContext";
import { useNavigate } from "react-router-dom";

function Revenue() {
  const { Revenue } = useContext(DriverContext);
  const [isShowRevenue, setIsShowRevenue] = useState(false);
  const navigate = useNavigate();

  // Xử lý khi người dùng nhấp vào phần tử và điều hướng đến trang khác
  const handleNavigateToAction = () => {
    navigate("/action"); // Điều hướng đến trang "action"
  };

  return (
    <div className="mt-10 ml-4 relative">
      <div
        className="p-4 bg-white rounded-full flex gap-3 shadow-md items-center cursor-pointer"
        onClick={() => setIsShowRevenue(!isShowRevenue)}
      >
        {isShowRevenue ? (
          <img src="/close.svg" alt="chart" className="w-4 h-4" />
        ) : (
          <>
            <img src="/chart.svg" alt="chart" className="w-4 h-4" />
            <span>Thu nhập</span>
          </>
        )}
      </div>
      {isShowRevenue ? (
        <div
          className="flex justify-between items-center p-3 bg-white absolute top-[100%] w-full rounded-md min-w-[140px] cursor-pointer"
          onClick={handleNavigateToAction}
        >
          <div className="">
            <p className="text-sm">Thu nhập</p>
            <p>
              {(Revenue * 1000).toLocaleString("vi-VN", {
                style: "currency",
                currency: "VND",
              })}
            </p>
          </div>
          <div>
            <img src="/right.svg" alt="" className="w-5 h-5" />
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default Revenue;
