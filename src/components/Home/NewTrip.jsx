import React, { useState, useEffect, useContext } from "react";
import { DriverContext } from "../../context/DriverContext";

function NewTrip() {
  const { setNewTrip, newTrip, handleAcceptTrip } = useContext(DriverContext);
  const [countdown, setCountdown] = useState(20);
  const [isActive, setIsActive] = useState(true);

  useEffect(() => {
    let interval;
    if (isActive && countdown > 0) {
      interval = setInterval(() => {
        setCountdown((prev) => prev - 1);
      }, 1000);
    } else if (countdown === 0) {
      setIsActive(false);
      setNewTrip();
    }

    return () => clearInterval(interval);
  }, [isActive, countdown]);

  return (
    <div className="absolute inset-x-0 bottom-0 z-20 bg-white px-3 py-5">
      <div>
        <h1 className="text-xl font-semibold">Có chuyến đi mới</h1>
      </div>
      <div className="flex items-center gap-3 mt-3">
        <div>
          <img
            src="/customer.svg"
            alt="avatar customer"
            className="w-16 h-16"
          />
        </div>
        <div className="flex justify-between grow items-center">
          <p>{newTrip?.NameCustomer}</p>
          <div className="flex flex-col">
            <span>{newTrip.price}k</span>
            <span>{newTrip.distance} km</span>
          </div>
        </div>
      </div>
      <div className="w-full flex justify-center mt-5">
        <div className="w-[98%] bg-blue-100 pb-3 rounded-sm">
          <p className="p-2">{newTrip?.source}</p>
          <p className="p-2">{newTrip?.destination}</p>
          <div className="text-center" onClick={handleAcceptTrip}>
            <button className="text-xl font-semibold">
              Đồng ý nhận chuyến ({countdown}s)
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NewTrip;
