import React from "react";
import { useNavigate } from "react-router-dom";

function DriverSetting() {
  const navigate = useNavigate();
  return (
    <div className="mt-10 mr-4" onClick={() => navigate("/profile")}>
      <img
        src="/DriverProfile.svg"
        alt="driver Profile"
        className="w-16 h-16"
      />
    </div>
  );
}

export default DriverSetting;
