import React, { useContext } from "react";
import { DriverContext } from "../../context/DriverContext";

function InforTrip() {
  const { currentTrip, handleCompleteTrip, handleCancelTrip } =
    useContext(DriverContext);
  return (
    <div className="absolute inset-x-0 bottom-0 z-20 bg-white px-3 py-5">
      <div className="flex items-center gap-3 mt-3">
        <div>
          <img
            src="/customer.svg"
            alt="avatar customer"
            className="w-16 h-16"
          />
        </div>
        <div className="flex justify-between grow items-center">
          <div>
            <p className="font-semibold text-xl">{currentTrip?.NameCustomer}</p>
            <div className="flex gap-3">
              <div className="border border-gray-200 p-3 rounded-md">
                <img src="/phone.svg" alt="phone" className="w-5 h-5" />
              </div>
              <div className="border border-gray-200 p-3 rounded-md">
                <img src="/message.svg" alt="message" className="w-5 h-5" />
              </div>
            </div>
          </div>
          <div className="flex flex-col">
            <span>{currentTrip.price}k</span>
            <span>{currentTrip.distance} km</span>
          </div>
        </div>
      </div>
      <div className="w-full flex justify-center mt-5">
        <div className="w-[98%] bg-blue-100 pb-3 rounded-sm">
          <p className="p-2">{currentTrip?.source}</p>
          <p className="p-2">{currentTrip?.destination}</p>
          <div className="flex justify-center gap-4 px-2 ">
            <button
              className="text-xl font-semibold bg-red-400 w-[20%] p-2 rounded-md"
              onClick={() => handleCancelTrip()}
            >
              Hủy
            </button>
            <button
              className="text-xl font-semibold bg-blue-400 w-[95%] p-2 rounded-md"
              onClick={() => handleCompleteTrip()}
            >
              Hoàn thành chuyến đi
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default InforTrip;
